Prinsip-prinsip yang perlu diketahui, dipahami, dan diterapkan:
- Coding convention sesuai ketentuan tim/perusahaan
- Plan before you code
- Think how task is supposed to get done
- SOLID principles
- Broken windows theory 
- Boyscout rule
- “Make it work. Make it right. Make it fast” (In that order!) - Kent Beck

Terkait Coding Convention, silakan minta dokumen Coding Convention milik Sunwell